﻿using CollectionsLINQ.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace CollectionsLINQ.Services
{
    class TasksService
    {
        public TasksService()
        {

        }

        public async Task<ICollection<Models.Task>> GetAllTasks()
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://bsa20.azurewebsites.net/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var answer = await client.GetStringAsync("api/Tasks");
                return JsonConvert.DeserializeObject<ICollection<Models.Task>>(answer);
            }
        }

        public async Task<Models.Task> GetTaskById(int id)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://bsa20.azurewebsites.net/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var answer = await client.GetStringAsync($"api/Tasks/{id}");
                return JsonConvert.DeserializeObject<Models.Task>(answer);
            }
        }
    }
}
