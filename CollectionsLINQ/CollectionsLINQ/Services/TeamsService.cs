﻿using CollectionsLINQ.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace CollectionsLINQ.Services
{
    class TeamsService
    {
        public TeamsService()
        {

        }

        public async Task<ICollection<Team>> GetAllTeams()
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://bsa20.azurewebsites.net/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var answer = await client.GetStringAsync("api/Teams");
                return JsonConvert.DeserializeObject<ICollection<Team>>(answer);
            }
        }

        public async Task<Team> GetTeamById(int id)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://bsa20.azurewebsites.net/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var answer = await client.GetStringAsync($"api/Teams/{id}"); 
                return JsonConvert.DeserializeObject<Team>(answer);
            }
        }
    }
}