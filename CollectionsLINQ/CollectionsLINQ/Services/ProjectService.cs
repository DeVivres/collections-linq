﻿using CollectionsLINQ.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace CollectionsLINQ.Services
{
    class ProjectService
    {
        public ProjectService()
        {

        }

        public async Task<ICollection<Project>> GetAllProjects()
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://bsa20.azurewebsites.net/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var answer = await client.GetStringAsync("api/Projects/");
                return JsonConvert.DeserializeObject<List<Project>>(answer);
            }
        }
            
        public async Task<Project> GetProjectById(int id)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://bsa20.azurewebsites.net/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var answer = await client.GetStringAsync($"api/Projects/{id}");
                return JsonConvert.DeserializeObject<Project>(answer);
            }
        }
    }
}
