﻿using CollectionsLINQ.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace CollectionsLINQ.Services
{
    class TaskStatesService
    {
        public TaskStatesService()
        {

        }

        public async Task<ICollection<TaskStateModel>> GetTasksStates()
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://bsa20.azurewebsites.net/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var answer = await client.GetStringAsync("api/TaskStates");
                return JsonConvert.DeserializeObject<ICollection<TaskStateModel>>(answer);
            }
        }
    }
}