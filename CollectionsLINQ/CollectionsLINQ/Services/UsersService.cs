﻿using CollectionsLINQ.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace CollectionsLINQ.Services
{
    class UsersService
    {
        public UsersService() 
        { 
            
        }

        public async Task<ICollection<User>> GetAllUsers()
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://bsa20.azurewebsites.net/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var answer = await client.GetStringAsync("api/Users");
                return JsonConvert.DeserializeObject<ICollection<User>>(answer);
            }
        }

        public async Task<User> GetUserById(int id)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://bsa20.azurewebsites.net/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var answer = await client.GetStringAsync($"api/Users/{id}");
                return JsonConvert.DeserializeObject<User>(answer);
            }
        }
    }
}