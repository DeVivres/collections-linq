﻿using CollectionsLINQ.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CollectionsLINQ.Services
{
    class DataObject
    {
        private readonly ProjectService _projectService;
        private readonly TasksService _tasksService;
        private readonly TeamsService _teamsService;
        private readonly UsersService _usersService;

        public List<Project> Projects;
        public List<Task> Tasks;
        public List<Team> Teams;
        public List<User> Users;

        public DataObject()
        {
            _projectService = new ProjectService();
            _tasksService = new TasksService();
            _teamsService = new TeamsService();
            _usersService = new UsersService();

            Projects = _projectService.GetAllProjects().Result.ToList();
            Tasks = _tasksService.GetAllTasks().Result.ToList();
            Teams = _teamsService.GetAllTeams().Result.ToList();
            Users = _usersService.GetAllUsers().Result.ToList();
        }
    }
}
