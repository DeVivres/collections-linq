﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CollectionsLINQ.Models
{
    public class TaskStateModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("value")]
        public string? Value { get; set; }

        public override string ToString()
        {
            return string.Format($"Task State Id {Id}\n" +
                                 $"Value: {Value}\n");
        }
    }
}
