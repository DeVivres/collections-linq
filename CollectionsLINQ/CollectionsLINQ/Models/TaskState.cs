﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CollectionsLINQ.Models
{
    public enum TaskState
    {
        Created,
        Started,
        Finished,
        Canceled
    }
}
