﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CollectionsLINQ.Models
{
    public class Team
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string? Name { get; set; }

        [JsonProperty("createdAt")]
        public DateTime CreatedAt { get; set; }

        public override string ToString()
        {
            return string.Format($"Team Id {Id}\n" +
                                 $"Team Name: {Name}\n" +
                                 $"Created At: {CreatedAt}\n");
        }
    }
}
