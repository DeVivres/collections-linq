﻿using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Collections;
using CollectionsLINQ.Models;
using CollectionsLINQ.Services;
using System.Reflection.Metadata.Ecma335;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.Xml;
using System.Security.Cryptography.X509Certificates;

namespace CollectionsLINQ
{
    class Program
    {
        static void Main(string[] args)
        {
            var service = new MainService();
            var result1 = service.FirstRequest(1);
            var result2 = service.SecondRequest(4, 45);
            var result3 = service.ThirdRequest(5, 2020);
            var result4 = service.FourthRequest(10);
            var result5 = service.FifthRequest();
            Console.WriteLine("Please check the data by putting stop point on this Console.WriteLine()");
        }
    }
}


